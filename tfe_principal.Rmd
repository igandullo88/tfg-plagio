---
documentclass: book
principal: yes
forprint: yes
fontsize: 12pt
geometry: margin = 2.5cm
bibliography:
- bib/library.bib
- bib/paquetes.bib
metodobib: yes
biblio-style: plainnat
csl: methods-in-ecology-and-evolution.csl
link-citations: yes
output:
  pdf_document:
    toc: yes
    df_print: kable
    keep_tex: no
    number_sections: yes
    citation_package: natbib
    fig_caption: yes
    template: latex/templateMemoriaTFE.tex
    includes:
      before_body: portadas/latex_paginatitulo_modTFE.tex
  word_document:
    toc: yes
urlcolor: blue
header-includes:
- \setcitestyle{square,numbers}
- \usepackage{float}
- \usepackage{graphicx}
- \usepackage{sidecap}
- \newtheorem{defi}{Definición}
- \newtheorem{cuadro}{Cuadro}
- \newtheorem{ejem}{Ejemplo}
- \usepackage{amsmath}
- \usepackage{amssymb}
- \usepackage[dvipsnames]{xcolor}
- \restylefloat{table}
- \usepackage{framed}
- \usepackage{color}
- \usepackage{array}
- \usepackage{wrapfig}\definecolor{shadecolor}{RGB}{224,238,238}
---


---
nocite: | 
  @WinnowingAlgorithm, @repojplag, @wise1993string, @banchoff2008herramientas, 
  @Rsoft, @haskellsoft, @pysoft, @pycode, @AST, @@hage2013plagiarism, @kammer2011plagiarism,
  @helium, @CoefSorensen, @ComparisonWinnowing, @Winnowing, @MOSS, @Jplag, @R-SimilaR
...


```{r global_options, include=FALSE}
#Sys.setlocale('LC_ALL','C') # corrige problema con (ocasionaba problemas con acentos en fig.cap)
options(kableExtra.latex.load_packages = F)
#options(tinytex.latexmk.emulation = FALSE)
knitr::opts_chunk$set(fig.path = 'figurasR/',
                      echo = TRUE, warning = FALSE, message = FALSE,
                      fig.pos="H",fig.align="center",out.width="95%",
                      cache=FALSE) # 
knitr::write_bib(c("SimilaR"),
                 file="bib/paquetes.bib", width = 60)
```


<!-- Indentar el texto al inicio de cada nuevo párrafo -->
\setlength{\parindent}{1em}

\pagestyle{fancy}
\ifdefined\ifdoblecara
\fancyhead[LE,RO]{}
\fancyhead[LO,RE]{}
\else
\fancyhead[RO]{}
\fancyhead[LO]{}
\fi
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\pagenumbering{roman}

<!-- \setcounter{tocdepth}{4} -->
<!-- \subpdfbookmark{Índice General}{indice} -->
<!-- \tableofcontents -->


\cleardoublepage   
<!--  \clearpage -->
\section*{Prólogo}
\addcontentsline{toc}{section}{Prólogo}


```{r child = 'prologo.Rmd'}
```


\cleardoublepage   
<!--  \clearpage -->
\section*{Resumen}
\addcontentsline{toc}{section}{Resumen}

```{r child = 'resumen.Rmd'}
```


\clearpage
\section*{Abstract}
\addcontentsline{toc}{section}{Abstract}

```{r child = 'abstract.Rmd'}
```


\cleardoublepage   
\listoffigures
\addcontentsline{toc}{section}{Índice de Figuras}

\cleardoublepage   

\listoftables
<!-- \addcontentsline{toc}{section}{Índice de Cuadros} -->
\addcontentsline{toc}{section}{Índice de Tablas}

\cleardoublepage   

\pagenumbering{arabic}

\ifdefined\ifdoblecara
\fancyhead[LE,RO]{\scriptsize\rightmark}
\fancyfoot[LO,RE]{\scriptsize\slshape \leftmark}
\fancyfoot[C]{}
\fancyfoot[LE,RO]{\footnotesize\thepage}
\else
\fancyhead[RO]{\scriptsize\rightmark}
\fancyfoot[LO]{\scriptsize\slshape \leftmark}
\fancyfoot[C]{}
\fancyfoot[RO]{\footnotesize\thepage}
\fi

\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

```{r child = 'capitulo01.Rmd'}
```

\FloatBarrier

```{r child = 'capitulo02.Rmd'}
```

\FloatBarrier

```{r child = 'capitulo03.Rmd'}
```

\FloatBarrier

```{r child = 'capitulo04.Rmd'}
```


<!-- 
Descomentarlo para incluir el capítulo 5 el cual contiene utilidades de ejemplo
para copiar y pegar.
Atención: necesitará la instalación de algunos paquetes R 
que puede no tener instalados. Son:
install.packages(c("ggplot2","kableExtra","dplyr"))
-->


\FloatBarrier

```{r child = 'capitulo05.Rmd'}
```



\FloatBarrier

```{r child = 'capitulo06.Rmd'}
```      


\FloatBarrier

\appendix

```{r child = 'apendice01.Rmd'}
```



```{r child = 'apendice02.Rmd'}
```


\FloatBarrier
\cleardoublepage

\ifdefined\ifdoblecara
  \fancyhead[LE,RO]{}
  \fancyfoot[LO,RE]{}
  \fancyhead[CO,CE]{Bibliografía}
\else
  \fancyhead[RO]{}
  \fancyfoot[LO]{}
  \fancyhead[CO]{Bibliografía}
\fi


\ifdefined\ifcitapandoc

\hypertarget{bibliografuxeda}{%
\chapter*{Bibliografía}\label{bibliografuxeda}}
\addcontentsline{toc}{chapter}{Bibliografía}


\else

<!-- Si "metodobib: true", modificar este "nocite:"  -->
<!-- Si "metodobib: false", modificar el "nocite:" del inicio de este fichero  -->

<!-- \nocite{*} -->
\nocite{R-base,Author2013,Techopedia, R-SimilaR}

\fi 
