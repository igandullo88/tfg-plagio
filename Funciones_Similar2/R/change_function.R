change_function = function(txt){
  # Convierte txt a data.frame y añade la columna line
  df_txt = txt %>%
    dplyr::as_data_frame()%>%
    dplyr::mutate(line = dplyr::row_number())

  # Elimina los espacios en blanco, se realiza la particion de df_txt$value por el patterm '#' y
  # nos quedamos con la parte de código, finalmente eliminamos las filas en blanco.
  df_txt2= df_txt %>%
    dplyr::mutate(value = stringr::str_remove_all(string = value, pattern = "[[:space:]]"))%>%
    dplyr::mutate( value = stringr::str_split_fixed(value,"#",2)[,1]) %>%
    dplyr::filter(value !="")

  # Se crean 4 nuevas variables. Se hacen dos particiones "=fucnticon" y "<-function". 
  # 2 variables (is_op1 = parte anterior a '=function', no_op1 [parte posterior a "=function"])  
  # y otras 2 (is_op2, no_op2) [igual que antes pero con "<-function"].
  n_funciones = df_txt2 %>%
    dplyr::mutate(is_op1 =stringr::str_split_fixed(value,pattern = "=function", 2)[,1],
                  no_op1 =stringr::str_split_fixed(value,pattern = "=function", 2)[,2],
                  is_op2 =stringr::str_split_fixed(value,pattern = "<-function", 2)[,1],
                  no_op2 =stringr::str_split_fixed(value,pattern = "<-function", 2)[,2])
  
  # Capturamos los nombres de las funciones definidas con el operador =.
  Operador1 = n_funciones %>%
    dplyr::select(is_op1, no_op1,line)%>%
    dplyr::filter(no_op1 != "" ) %>% 
    dplyr::select(operador = is_op1, line)
  # Capturamos los nombres de las funciones definidas con el operador <-.
  Operador2 = n_funciones %>%
    dplyr::select(is_op2, no_op2,line)%>%
    dplyr::filter(no_op2 != "" ) %>%
    dplyr::select(operador = is_op2, line)

  # Agrupamos las variables Operador 1 y Operador 2
  Operadores = dplyr::bind_rows(Operador1, Operador2)%>%
    dplyr::arrange(line)
  
  # Este paso, se efectúa para eliminar posibles capturas erroneas de los nombres de funciones
  Operadores = Operadores %>%
    dplyr::mutate(no_oper = stringr::str_split_fixed(operador, "\\(",2)[,2])%>%
    dplyr::filter(no_oper == "")%>%
    dplyr::select(1,2)

  # Se capturan los nombres de las funciones repetidos
  rep_func = Operadores %>%
    dplyr::group_by(operador)%>%
    dplyr::count() %>%
    dplyr::filter(n>1) %>%
    dplyr::select(1) %>%
    unlist()
  
  # Se filtran de  Operadores las funciones repetidas
  cambiar_op = Operadores %>%
    dplyr::filter(operador %in% rep_func)

  if(nrow(cambiar_op)>0){ # Si cambiar_op tiene al menos 1 fila
    uniq_op = unique(cambiar_op$operador)

    camb_func = list() # Se crea una lista, para cada función a la que se debe añadir subíndices.
    
    for(i in 1:length(uniq_op)){
      nombres = uniq_op[[i]]
      df_func = cambiar_op %>% # Se filtra un data.frame para la función i-ésima
        dplyr::filter(operador == uniq_op[i])
      for(j in 1:(nrow(df_func)-1)){
        nombres = c(nombres, paste0(uniq_op[i],"_",j+1)) # En una columna, se añade los sunbíndices 
      }
       
      camb_func[[i]] = df_func %>%
        dplyr::mutate(nuevos = nombres) # Se añade la columna nueva, al data.frame df_func
    }

    # Con map_df convertimos todas las listas del paso anterior a data.frame y lo oredenamos por la fila line.
    df_camb = purrr::map_df(camb_func, ~.x) %>%
      dplyr::arrange(line)
    
    # Eliminamos las filas a las que no se han añadido subíndices.
    for(i in 1:length(uniq_op)) df_camb = df_camb %>% dplyr::filter(nuevos != uniq_op[i])

    # Se reemplaza los nombres de las variables redefinidas con los nuevos nombres con subíndices.
        
    for(i in 1:nrow(df_camb)){
      # Para ello,  obtenemos el valor í-esimo de la linea de df_camb.
      # Reemplazamos en df_txt[df_camb$line[i],] el valor del operador i-ésimo de de_camb
      # por el valor i-ésimo de df_camb$nuevos.
      df_txt[df_camb$line[i], 1] = stringr::str_replace(
        string = df_txt[df_camb$line[i], 1],
        pattern = df_camb$operador[i],
        replacement = df_camb$nuevos[i]
      )
    }

    return(df_txt)
  }else{
    return(df_txt)
  }
}

