## ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

## ###################################################################
## Ejercicio 1
## ###################################################################

load("repaso1.Rdata")

## El vector telefonos contiene el número de líneas telefónicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.

## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# Se crean los factores con la función factor
factor.lugar=factor(lugar)
factor.fecha=factor(fecha)
levels(factor.fecha)
levels(factor.lugar)

## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.

telefonos
telefonos1961=telefonos[c(7,14,21,29,35,42,49)]
names(telefonos1961)=factor.lugar
telefonos1961 # vector del año 1961

lugar.ano=matrix(telefonos,nrow = length(factor.fecha),ncol = length(factor.lugar),dimnames = list(factor.fecha,factor.lugar))
lugar.ano # matriz del todos los numeros en funcion del año y del lugar.

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.

# Con un bucle for se recorre la seq(1:7), y se va calculando los porcentajes (dato / suma(datos lugar)) * 100
porcentaje1961=vector()
for (i in seq(1:7))
  porcentaje1961[i]=100*lugar.ano[7,i]/sum(lugar.ano[,i])
names(porcentaje1961)=factor.lugar
porcentaje1961

# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:

# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:
library(readr)
estadisticas_vitales=read.csv("Dataset/Sintetico/estadisticas_vitales.txt",sep = "")
colnames(estadisticas_vitales)=c("Población","Nacimientos","Defunciones")
Año = rownames(estadisticas_vitales)

# 1. La población en el año 1992.


estadisticas_vitales["1992","Población"]# Indicamos que la fila deseado es "Año==1992"
                                        # Columna 'Población'
# También, se puede hacer con la función subset, a la que se le proporciona el marco de datos
# la condición 'rownames(estadisticas_vitales)==1992' y la columna que se desea obtener select = Población
subset(estadisticas_vitales,rownames(estadisticas_vitales)==1992 ,select = Población)

# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".


crecimientoAño= function(a){ # Nombre de la función como dicta el enunciado
  if (1947<a & a<1997) { # Si cumple la condición 1947<a  y a<1997
    # Salida del crecimiento vegetatio = nacimientos en el año a menos defunciones en el año a
    (estadisticas_vitales[Año == a,"Nacimientos"])-(estadisticas_vitales[Año == a,"Defunciones"])
  }else{
    # Sino cumple la condición, salida de la siguiente frase.
    "Sólo disponibles para los años entre 1948 y 1996"
  }
}

# Comprobaciones
crecimientoAño(1952)
crecimientoAño(1997)

# 3. El número de nacimientos desde el primer hasta el último año.

# Aplicamos la función sum a la columna Nacimientos
sum(estadisticas_vitales[,"Nacimientos"])
# También, se puede selecionar los datos con la función subset como vimos antes, y se le aplica sum
sum(subset(estadisticas_vitales, select = Nacimientos))

# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.

# Al marco de datos aplicamos en el selector de fila la condición "1960"
estadisticas_vitales["1960",]
subset(estadisticas_vitales,Año==1960)

# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.

# Se aplica la función subset al marco de datos aplicamos en el selector de fila la condición '(Año >= 1950 & Año <= 1989)'

subset(estadisticas_vitales,Año> 1949 & Año<1990)

# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.

# Al marco de datos aplicamos en el selector de fila la condición 'Año %in% c(1950,1960,1970,1980,1990)' y en el de columna
# -2 ya que se devuelve Nacimientos y Defunciones

i=c("1950","1960","1970","1980","1990")
estadisticas_vitales[Año %in% i, -2]
       
