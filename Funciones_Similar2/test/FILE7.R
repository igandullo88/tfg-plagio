# ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################
## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

datos = load("repaso1.RData")

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el número de líneas telefónicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.

## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# Se crea una matrix con los tres vectores como se indica en el enunciado, siendo factor1a el factor de lugar 
# y factor1b el factor de fecha.
tel = matrix(telefonos,ncol=7,byrow=T)
(factor1a = cbind(lugar,tel))
(factor1b = t(rbind(fecha,tel)))

## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.

# Se selecciona los datos que cumple que factor1b =="1961"
(telefonos1961 = rbind(c("lugar",lugar),factor1b[factor1b[,1]=="1961"]))

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.

# Se calculan los porcentajes

x = factor1b[factor1b[,1]=="1961",]
y = x[2:length(x)]
p = as.numeric(y)/sum(as.numeric(y))

(porcentaje1961 = rbind(c("lugar",lugar),c("1961",round(p,2))))




# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:

# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:

# Lectura de datos

datos = read.table("estadisticas_vitales.txt",encoding = "UTF-8")
datos

# 1. La población en el año 1992.

(pob1992 = datos["1992",1])

# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".

# Función del crecimiento vegetativo. 'res' es el crecimiento vegetativo del año x,
# donde el crecimiento vegetativo es:
# el dato de nacimientos del año x - el dato de defunciones del año x.

crecimientoAño = function(año){
  if (año %in% 1948:1996){
    d = datos[as.character(año),]
    res = paste("El crecimiento vegetativo del año",año,"es de",d[2]-d[3])
    print(res)
    return(res)
  }
  else {
    print("Sólo disponibles para los años entre 1948 y 1996")
  }
}
for (i in 1948:1996){
  crecimientoAño(i)  
}

# 3. El número de nacimientos desde el primer hasta el último año.

# Columna Nacimientos
datos$Nacimientos

# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.

# Marco de datos donde rownames es igual a "1960"
datos["1960",]

# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.

# Del marco de datos se escogen todas las columnas que donde los valores de rownames están entre
# 1950 y 1989
datos[paste(1950:1989),]

# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.

# Del marco de datos se escogen todas las columnas que donde los valores de rownames están en
# el conjunto c("1950","1960","1970","1980","1990")
ind = c(1950, 1960, 1970, 1980, 1990)
datos[paste(ind),2:3]
