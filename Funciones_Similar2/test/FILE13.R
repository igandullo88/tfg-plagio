## ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el número de líneas telefónicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.

## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.

# En primer lugar, vamos a cargar los datos

load("repaso1.RData")

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# Tenemos 49 valores de teléfonos, por tanto, al tener tanto 7 fechas como lugares, vamos
# a repetir estos lugares y fechas formando así vectores cuya longitud es 49.

# En primer lugar creamos un vector donde repetimos 7 veces cada una de los lugares, 
# mediante la función rep utilizando each, y posteriormente lo pasamos a factor mediante
# la función as.factor

lugar2=rep(lugar,each=7);lugar2
factor_lugar=as.factor(lugar2)
factor_lugar

# Ahora hacemos lo mismo, pero con las fechas, donde aquí vamos a repetir la secuencia 
# de los 7 valores que corresponden a los 7 años, y posteriormente lo pasamos a factor
# mediante la función as.factor

fecha2=rep(fecha,7);fecha2
factor_fecha=as.factor(fecha2)
factor_fecha

## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.

telefonos

# Ahora vamos a tomar los teléfonos de la fecha 1961

telefonos1961 <- telefonos[factor_fecha=="1961"]
names(telefonos1961) <- lugar #Le asignamos los valores de lugar, que se corresponden
#ordenadamente con los de telefonos1961
telefonos1961 #Ya tenemos el vector creado pedido

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.

#Tomamos el vector telefonos 1961 y lo dividimos entre la suma de todos los valores de
#ese vector, de esta forma conseguimos los porcentajes al multiplicarlo por 100.

porcentaje1961=telefonos1961/sum(telefonos1961)*100
porcentaje1961

#Podemos observar que el mayor porcentaje de líneas telefónicas en el año 1961
#se da en N.Amer con más de un 50%. En segundo lugar está Europa con un 30% 
#aproximadamente, el resto de lugares, tienen porcentajes bastante pobres.

#Podemos hacer un grafico de barras con la orden barplot y ver graficamente los porcentajes
barplot(porcentaje1961)

# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:

# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:

# En primer lugar, leemos los datos mediante la función read.table

datos=read.csv("estadisticas_vitales.txt",encoding = "UTF-8",sep="")
summary(datos) #Breve resumen de los datos
dim(datos) #49 observaciones de 3 variables distintas
str(datos) #Todas las variables tienen valores enteros

#################################
# 1. La población en el año 1992.
#################################

# Con la introducción de la variable año, va a ser mucho más fácil seleccionar la población del año
# que queramos, en particular, la población del año 1992: 

poblacion1992=datos["1992",1] #Tomamos únicamente la primera columna de la fila
# correspondiente al año 1992, que es la que nos ofrece la población en dicho año.

cat("La población en el año 1992 es:", poblacion1992)

#####################################################################################################
# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".

## Definimos la función con la instrucción "function()" y le exigimos 
## el argumento "ano", para el año
#####################################################################################################

# El crecimiento vegetativo es la diferencia entre los nacimientos y las defunciones en una determinada 
# población dividido entre el número total de la población y multiplicado por 100, sabiendo esto, 
# definimos la función:

crecimientoAño = function(año){
  if (año<=1996 & año>=1948){
    año=as.character(año)
    crecimiento_vegetativo=(datos[año,2]-datos[año,3])/datos[año,1]*100
    print(paste("Crecimiento Vegetativo del año", año ,"es" , crecimiento_vegetativo))
  }
  else {
    print("No disponible. Sólo disponibles para los años entre 1948 y 1996")
  }
}

# A la función le damos como valor de entrada un año. 
# Si ese año se encuentra entre el mínimo y el máximo valor de los años que se encuentran 
# en nuestros datos que son 1948 y 1996 respectivamente, entonces calcula el crecimiento 
# vegetativo y nos da su valor. En caso contrario, nos devuelve la frase "Sólo disponibles
# para los años entre 1948 y 1996"

# Probemos ahora que efectivamente hace eso: 

crecimientoAño(1997) #Nos dice que no está disponible

crecimientoAño(1947) #Nos dice que está disponible

crecimientoAño(1990) #Nos devuelve el valor del crecimiento vegetativo pues se encuentra
# entre los años que se disponen.

# Tambíen podemos ponerlo así: 

crecimientoAño("1990")

##################################################################
# 3. El número de nacimientos desde el primer hasta el último año.
##################################################################

# Para ello, nos es suficiente seleccionar la columna donde se encuentran los nacimientos, 
# es decir, la columna número 2. Se haría de la siguiente forma: 

datos[,2]

Nacimientos=datos[,2]; Nacimientos 

# Si por otro lado, lo que realmente se pidiese en este ejercicio fuese el número
# total de nacimientos, se haría: 

cat("El número total de nacimientos en los años registrados es", sum(datos[,2]))

####################################################################################
# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.
####################################################################################

# Para ello, seleccionamos la fila que tiene los datos correspondientes al año 1960

Datos1960=datos["1960",]
Datos1960

####################################################################################
# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.
####################################################################################

#Vamos a saleccionar las filas correspondientes a los años pedidos, para
#ello creamos un vector con dichos años, el cual pasamos a tipo character

Datos1950_1989=datos[as.character(1950:1989),]
Datos1950_1989

######################################################################################
# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.
######################################################################################

#Hacemos lo mismo que en ele ejercicio anterior, pero aquí, seleccionamos 
#las columnas 2 y 3, correspondientes a los nacimientos y defunciones.

datos_ejer6=datos[as.character(seq(1950,1990,by=10)),c(2:3)]
datos_ejer6

