f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################
## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

#Cargamos los datos 

load("repaso1.RData")   
lugar
factor.zona<-factor(rep(lugar,each=7),levels = c("N.Amer" ,  "Europe" ,  "Asia"    , "S.Amer" ,  "Oceania",  "Africa"   ,"Mid.Amer"))
factor.zona   

#usamos el comando factor para crear el factor y levels para los niveles
#las repeticiones se llevan a cabo cada 7 porque nuestro vector es de longitud 7 y lo queremos hasta 49

fecha
factor.fecha<-factor(rep(fecha,each=7))
factor.fecha


#de igual manera que el anterior 
## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.
## ###################################################################



telefonos1961<-telefonos[factor.fecha==1961]
telefonos1961

names(telefonos1961)<-levels(c("N.Amer" ,  "Europe"  , "Asia" ,    "S.Amer"   ,"Oceania" , "Africa" ,  "Mid.Amer"))
telefonos1961

#usamos el comando names para nombrar datos

## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

porcentaje1961 <- (telefonos1961/sum(telefonos1961))*100
porcentaje1961

#henos aplicado la definicion de porcentaje 


# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:


#Lectura de datos 

datos<-read.table("estadisticas_vitales.txt",header=T)
datos

attach(datos)

# 1. La poblacion en el ano 1992.

pob1992<-datos["1992",1]     # 1992 es un caracter , por tanto entre comillas 
pob1992                      # y 1 porque queremos la poblacion que es la primera columna



# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".



}
crecimientoAno<-function(ano){
  anos<-row.names(datos)     #row.names ->  nombre de la fila
    if(ano >=min(anos)&& ano<=max(anos)){
      return(Nacimientos[row.names(datos)==ano]-Defunciones[row.names(datos)==ano])   #el crecimiento vegetativo son los nacimientos menos defunciones
    }
    else {
      print("Solo disponibles para los anos entre 1948 y 1996")    #print para imprimir  por pantalla 
    }
}
f2= function(){


#Comprobacion
crecimientoAno(1972) 
crecimientoAno(2000)


# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

nacimientos<-datos[,2]     #selecciono todos los nacimientos
nacimientos

sum(nacimientos)            #y los sumo 

#otra forma 

attach(datos)
sum(Nacimientos)

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

datos["1960",]        #si no selecciono ninguna columna R por defecto las coge todas

# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

datos[row.names(datos)>=1950&row.names(datos)<=1989, ]    

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

datos[c("1950","1960","1970","1980","1990"),c(2,3)]


}
