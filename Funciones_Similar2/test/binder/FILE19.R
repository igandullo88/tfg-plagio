f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

load("repaso1.RData")

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# Expresamos la variable lugar como un factor donde los distintos niveles seran las 
# distintas zonas y repetimos cada uno de los lugares 7 veces cada uno 
(factor.lugar = rep(factor(lugar), each = 7))

# Analogamente para la variable fecha pero repitiendo la secuencia de fechas 7 veces
(factor.fecha = rep(factor(fecha),7))

# Hemos usado esa configuracion del "rep" en cada una de las expresiones de arriba ya que
# nos dicen que los datos estan organizados por zonas, y por cada zona, por anos. 


## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.
## ###################################################################

# Seleccionamos los telefonos cuyas posiciones corresponden al ano 1961
telefonos1961 <- telefonos[factor.fecha == 1961]

# Analogamente, tomamos los lugares cuyas posiciones corresponden al ano
# 1961 y asignamos estos nombres al vector anterior:  
names(telefonos1961) <- factor.lugar[factor.fecha == 1961]

# Y el resultamos que obtenemos es:
telefonos1961


## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

(porcentaje1961 <- 100 * telefonos1961/sum(telefonos1961))

# Podemos ver estos porcentajes en un diagrama de sectores:
pie(porcentaje1961, labels = paste(names(porcentaje1961), round(porcentaje1961,2), "%"),
    col = rainbow(length(porcentaje1961)))


# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:

# Leemos los datos usando la funcion "read.csv"
datos <- read.csv("estadisticas_vitales.txt", fileEncoding = "UTF-8", header = T, sep = "")
datos



#########################################################################################################
# 1. La poblacion en el ano 1992.
#########################################################################################################

# Como las nombres de las filas son los anos, podemos acceder a los datos de la fila correspondiente
# a la informacion de 1992. Nos quedaremos con la primera variable que corresponde a la poblacion
poblacion.1992 <- datos["1992",1]
poblacion.1992

#########################################################################################################
# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".
#########################################################################################################

}# Definimos la funcion crecimientoAno segun nos indican en el enunciado:
crecimientoAno <- function(ano){
  
  if(ano<1948 | ano>1996){
    # Mostramos un mensaje de error si el ano introducido como argumento de la funcion no esta en el 
    # rango de anos de los que disponemos de informacion
    stop("Solo disponibles para los anos entre 1948 y 1996") 
    
  } else{
    # Si ano introducido esta entre 1948 y 1996, devolvemos la poblacion en ese ano
    datos[as.character(ano),1]
    }
}
f2= function(){

#########################################################################################################
# 3. El numero de nacimientos desde el primer hasta el ultimo ano.
#########################################################################################################

# Para calcular el numero de nacimientos desde el primer hasta el ultimo ano, sumamos los nacimientos
# de cada uno de los anos. Para ello extraemos la segunda columna del conjunto de datos que contiene
# la variable "nacimientos" y sumamos:
total_nacimientos <- sum(datos[,2])
total_nacimientos


#########################################################################################################
# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.
#########################################################################################################

# Accedemos a la informacion de la fila cuyo nombre es el ano 1960:
datos.1960 <- datos["1960",]
datos.1960


#########################################################################################################
# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.
#########################################################################################################

# Creamos una sucesion con los anos de los que queremos tener la informacion:
anos.de.1950.a.1989 <- 1950:1989
anos.de.1950.a.1989

# Extraemos los datos para dichos anos. Tenemos que expresar los anos como un caracteres
# ya que los nombres de las filas no son numeros sino cadenas de caracteres.
datos.de.1950.a.1989 <- datos[as.character(anos.de.1950.a.1989),]
datos.de.1950.a.1989


#########################################################################################################
# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.
#########################################################################################################

# Como nos piden la informacion de anos distanciados en 10 anos podemos crear una secuencia
# en lugar de crear directamente un vector con esos anos:
fechas <- seq(1950, 1990, 10)
fechas

# Obtenemos la informacion de nacimientos y defunciones (segunda y tercera columna de la matriz de datos)
# de los anos seleccionados. De nuevo es necesario expresar los anos como caracteres.
datos.anos.seleccionados <- datos[as.character(fechas),]
datos.anos.seleccionados}
