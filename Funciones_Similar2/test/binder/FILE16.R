f1= function(){## ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

# Cargamos los datos.
load("repaso1.RData")

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de luineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# Creamos un vector con los lugares que corresponden a los telefonos y otro con las fechas.
zona <- rep(lugar, each = 7)
tiempo <- rep(fecha, 7)

# Creamos los factores del enunciado a partir del vector anterior.
factor.zona <- factor(zona)
factor.fecha <- factor(tiempo)
factor.zona
factor.fecha

## Si hacemos lo siguiente, vemos como los vectores creamos corresponden en efecto a los telefonos.
datos <- data.frame(telefonos, lugar = zona, fecha = tiempo)
datos

## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.

# Creamos el vector de los telefonos en el ano 1961, los nombramos con la zona correspondiente.
telefonos1961 <- telefonos[factor.fecha == 1961]
names(telefonos1961) <- factor.zona[factor.fecha == 1961]
telefonos1961

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

# Para hallar el vector del enunciado, dividimos el vector del apartado (2) por la suma de los
# valores del dicho vector.
porcentaje1961 <- telefonos1961 / sum(telefonos1961)
porcentaje1961

## A continuacion, creamos un diagrama de sectores correspondiente al vector de poncentajes.
pie(porcentaje1961, col = rainbow(7))
## ###################################################################
## Ejercicios de repaso de R.
## ###################################################################

## El fichero estadisticas_vitales.txt contiene una tabla de datos relativos a las estadisticas
## vitales genericas para la poblacion espanola. La tabla consta de 49 filas, cada una de las cuales
## se corresponde con un ano, desde 1948 hasta 1996. Para cada fila existen 3 columnas que contienen
## la siguiente informacion:

## Poblacion total en ese ano.
## Numero de nacimientos.
## Numero de defunciones.

# Primero, vamos a cargar los datos. Viendo el conjunto de datos, vemos que estan separados por
# espacio y que hay que abrirlo con UTF-8 para que nos salga la tilde de Poblacion.
datos <- read.csv("estadisticas_vitales.txt", sep = "", fileEncoding = "UTF-8")

# Observaciones:
# Como la funcion read.csv nos una elemento cuya clase es un data.frame, y en la indicacion nos dice
# que usemos dicha funcion, vamos a hacer los ejercicios haciendo consultas al data.frame y no a
# vectores o factores. Si quisieramos hacerlo con vectores o factores, bastaria tomar tres vectores
# que fueran:
# * Poblacion <- datos[, 1].
# * Nacimientos <- datos[, 2].
# * Defunciones <- datos[, 3].
# Y hacer los ejercicios igual sabiendo que hallar el valor de un ano concreto n, equivale a hallar
# el valor del (n - 1947)-esimo de cada vector.

## ###################################################################
# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:
## ###################################################################
# 1. La poblacion en el ano 1992.

# Para hallarlo, tenemos en cuenta que las filas son los anos y las columnas las variables. Por
# tanto, se obtiene de la siguiente forma.
poblacion_1992 <- datos["1992", "Poblacion"]
poblacion_1992

# Observaciones:
# Tambien se podria haber obternido poniendo el numero de la fila y de la columna correspondiente,
# es decir, datos[45, 1] ya que 1992 - 1947 = 45

## ###################################################################
# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".

# Creamos una funcion que dado un ano, nos de el crecimiento vegetativo, es decir, la diferencia
# entre nacimientos y defunciones.
# Para ello, primero calculamos el numero de la fila correspondiente al ano al del que se quiere
# calcular el crecimiento vegetativo.
# Como sabemos que tenemos 49 anos porque lo dice el enunciado, si el numero de la fila resultante
# esta entre 0 y 49, podemos calcular sin problemas crecimiento vegetativo. Para ello, calculamos los
# nacimientos, las defunciones y los restamos. Por ultimo, lo imprimimos por pantalla y nos devuelve
# el valor.
# Sin embargo, si el numero de la fila correspondiente no esta entre 0 y 49, el "if" seria FALSE y
}# pasariamos al "else". En ese caso, se muestra por pantalla el mensaje pedido.
crecimientoAno <- function(n) {
  numero_fila <- n - 1947
  if (numero_fila > 0 && numero_fila < 50) {
    nacimientos <- datos[numero_fila, "Nacimientos"]
    defunciones <- datos[numero_fila, "Defunciones"]
    crecimiento_vegetativo <- nacimientos - defunciones
    return(crecimiento_vegetativo)
  }
  else {
    print("Solo disponibles para los anos entre 1948 y 1996")
  }
}
f2= function(){
# Observaciones:
# Estamos suponiendo que nos dan un numero al menos entero, ya que sino habria problemas a la hora
# de seleccionar las filas correspondientes.
# Podriamos haberlo hecho con as.character, pero como no la hemos visto en clase, no la uso.
# Podriamos poner en la linea anterior a "return(crecimiento_vegetativo)", lo siguiente para que
# imprimiera por pantalla una frase con mas informacion, pero como no lo pide y a lo mejor lo
# necesario es solo el numero, mejor lo quitamos:
# print(paste("El crecimiento vegetativo del ano", n, "es", crecimiento_vegetativo)).

## ###################################################################
# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

# Calculamos los nacimientos y los sumamos.
nacimientos <- sum(datos[, "Nacimientos"])
nacimientos

# Observaciones:
# Si lo que nos piden es el numero de nacimientos en cada ano, seria de la siguiente forma; poniendo
# los nombre para que se identifique cada valor:
# nacimientos <- datos[, "Nacimientos"]
# names(nacimientos) <- rownames(datos)
# nacimientos

## ###################################################################
# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

# Calculamos todos los datos correspondientes al ano 1960, recordando que los anos estan por filas.
datos_1960 <- datos["1960", ]
datos_1960

# Observaciones:
# Si queremos cada dato por separado, seria de la siguiente forma:
# poblacion_1960 <- datos["1960", "Poblacion"]
# nacimientos_1960 <- datos["1960", "Nacimientos"]
# defunciones_1960 <- datos["1960", "Defunciones"]
# poblacion_1960
# nacimientos_1960
# defunciones_1960

## ###################################################################
# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

# Calculamos todos los datos desde 1950 hasta 1989. Para ello, tenemos en cuenta que la fila
# correspondiente a 1950 es 1950 - 1947 = 3, y la de 1989 es 1989 - 1947 = 42.
# Finalmente, con la funcion "seq", obtenemos todos los valores desde 3 a 42.
fila_1950 <- 1950 - 1947
fila_1989 <- 1989 - 1947
numeros_1950_1989 <- seq(fila_1950, fila_1989)
datos_1950_1989 <- datos[numeros_1950_1989, ]
datos_1950_1989

# Observaciones:
# Igual que antes, si se pide por separado, la componente de la fila seria igual y la de la columna
# seria cada una por separado.
# Podriamos haberlo hecho con as.character, pero como no la hemos visto en clase, no la uso.

## ###################################################################
# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

# Caculamos los nacimientos y defunciones de los anos indicados, usando la funcion "seq", usando
# ahora la opcion "by", que la ponemos cada 10.
# Como los anos van de 10 en 10 y no se salta ningun ano en los datos, podemos hacerlo como antes.
# Como ya hemos calculado el numero correspondiente a la fila 1950, no lo volvemos a calcular.
# Por ultimo, nos piden solo nacimiento y defunciones, por tanto escribimos "-1" para indicar que no
# nos de la columna de Poblacion.
fila_1990 <- 1990 - 1947
numeros_1950_1990 <- seq(fila_1950, fila_1990, by = 10)
nac_y_def_1950_1990 <- datos[numeros_1950_1990, -1]
nac_y_def_1950_1990

# Observaciones:
# Podriamos haberlo hecho con as.character, pero como no la hemos visto en clase, no la uso.
# Tambien podriamos haberlo hecho poniendo en las filas un vector que fuera:
# c("1950", "1960", "1970", "1980", "1990").
# Igual que antes, si se pide por separado, la componente de la fila seria igual y la de la columna
}# seria cada una por separado.
