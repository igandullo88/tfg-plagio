f1= function(){## ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

library(DT)
library(purrr)
library(dplyr)
library(tibble)

load("repaso1.RData")

fecha
lugar
telefonos
parados.edad
parados.estudios

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.
## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# Se convierte en factor los vectores lugar y fecha de la forma que dicta en el enunciado
df = data.frame(expand.grid(as.factor(lugar),as.factor(fecha)),telefonos)
names(df) = c("Zona","Fecha","Telefonos")
df

tabla = datatable(cbind(lugar,fecha,telefonos))
tabla

## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.

# Se filtra del marco de datos donde 'Fecha == 1961'

telefonos1961 = df %>% 
  filter(Fecha == "1961") %>% 
  select(-"Fecha") %>% 
  deframe()
telefonos1961

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

# Se calcula las proporciones del vector telefonos1961
telefonos1961 %>% 
  map(function(x) 100*x/sum(telefonos1961)) %>%
  as_vector() 
  

# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:


library(dplyr)
library(tibble)

estadisticas_vitales = read.table("estadisticas_vitales.csv", skip = 1, sep = "")
names(estadisticas_vitales) = c("Ano","Poblacion","Nacimientos","Defunciones")
summary(estadisticas_vitales)
head(estadisticas_vitales)


# 1. La poblacion en el ano 1992.

# Sobre el marco de datos se aplica la funcion filter con la condicion 'Ano == 1992'
estadisticas_vitales %>% 
  filter(Ano == "1992")

# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".


ano = 1992
ano %in% df$Ano
df$Nacimientos

# Creamos la funcion 'crecimientoAno' a la que se le proporciona un marco de datos y un parametro 'ano'
# Si se cumple la condicion que el valor del parametro pertence a la columna Ano, se calcula el crecimiento
}# vegetativo (Nacimientos - Defunciones), en c.c. ' stop("Error: Solo disponibles para los anos entre 1948 y 1996")'
crecimientoAno <-  function(df,ano){
  if (ano %in% df$Ano) {
    pos = which(df$Ano==ano)
    cv = (df$Nacimientos[pos]-df$Defunciones[pos])/df$Poblacion[pos] *100
    paste("El crecimiento vegetativo del ano ",ano," es ",round(cv,2))
  } else {
    stop("Error: Solo disponibles para los anos entre 1948 y 1996")
  }
}
f2= function(){
crecimientoAno(estadisticas_vitales,1948)
crecimientoAno(estadisticas_vitales,1996)
crecimientoAno(estadisticas_vitales,2001)


# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

# Sobre el marco de datos, usamos la funcion summarise con la instruccion 'suma = sum(Nacimientos)'
# Devuelve la suma de la columna Nacimientos.

estadisticas_vitales %>% 
  summarise(suma = sum(Nacimientos))

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

# Sobre el marco de usamos filter donde en la columna Ano es igual a "1960"
estadisticas_vitales %>% 
  filter(Ano == "1960") 

# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

# Usamos filter con el parametro between(Ano,1950,1989). Devuelve el marco de datos en ese intervalo de tiempo
estadisticas_vitales %>% 
  filter(between(Ano,1950,1989))

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

# Sobre el marco de datos usamos la funcion filter donde en la columna Ano este el conjunto c("1950","1960","1970","1980","1990")

estadisticas_vitales %>% 
  filter(Ano %in% c("1950","1960","1970","1980","1990")) %>% 
  select(-"Poblacion")

}
