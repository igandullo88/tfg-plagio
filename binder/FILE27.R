f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################
## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

load("repaso1.RData")
lugar=factor(rep(lugar,each=7)) # Creamos un factor y repetimos cada dato del vector lugar 7 veces.
fecha=factor(rep(fecha,times=7)) # Creamos un factor y repetimos el vector fecha 7 veces
fecha

## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.
## ###################################################################

# Del vector telefonos obtenemos los datos donde 'fecha == "1961"' es TRUE
telefonos1961=telefonos[fecha=="1961"]
names(telefonos1961)=levels(lugar) # Asignamos nombres al nuevo vector. Usamos names() y se le proporciona los lvls de lugar
telefonos1961


## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

# Los porcentajes del vector 1961 se calculan  sobre el vector telefonos1961 de la siguiente forma
# (dato / sum(datos))
porcentaje1961=telefonos1961/sum(telefonos1961)*100
porcentaje1961

# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:

# 1. La poblacion en el ano 1992.

# Lectura de los datos
estadisticas_vitales <- read.table("estadisticas_vitales.txt",
                                   header = TRUE)
# Comprobaciones
str(estadisticas_vitales) #para comprobar que es un marco de datos

# Del vector Poblacion se escogen los datos donde 'rownames(estadisticas_vitales)=="1992"' es igual a TRUE

estadisticas_vitales$Poblacion[rownames(estadisticas_vitales)=="1992"]

attach(estadisticas_vitales) #Otra forma
Poblacion[rownames(estadisticas_vitales)=="1992"]

estadisticas_vitales["1992","Poblacion"] #Otra forma

# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".
}
crecimientoAno = function (x) {
  if (any(x==rownames(estadisticas_vitales))==TRUE) { 
    b = estadisticas_vitales$Nacimientos[rownames(estadisticas_vitales)==x]
    -estadisticas_vitales$Defunciones[rownames(estadisticas_vitales)==x]
    # El dolar y lo que esta detras se podria quitar porque hicimos el attach
    # es decir, podria quedar 'Nacimientos[rownames(estadisticas_vitales)==x]'
    print(b)
  }
  else {print('Solo disponibles para los anos entre 1948 y 1996')
}}
f2= function(){crecimientoAno(1928)
crecimientoAno(1995)

# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

# Se escoge la columna Nacimientos

estadisticas_vitales[,-c(1,3)]

Num_Nac=estadisticas_vitales[,"Nacimientos"] #Otra forma
names(Num_Nac)<-rownames(estadisticas_vitales)
Num_Nac

data.frame(Nacimientos,row.names = rownames(estadisticas_vitales)) #Otra forma de hacerlo

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

# Del marco de datos se escogen todas las columnas que donde el valor de rownames es igual a "1960"
estadisticas_vitales["1960",]

# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

# Del marco de datos se escogen todas las columnas que donde los valores de rownames estan entre
# 1950 y 1989
estadisticas_vitales[as.character(1950:1989),]
estadisticas_vitales[c(as.character(1950:1989)),]#Otra forma

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

# Del marco de datos se escogen todas las columnas que donde los valores de rownames estan entre
# los valores que proporciona la seq(1950,1990,by=10)

estadisticas_vitales[as.character(seq(1950,1990,by=10)),-1]

}
