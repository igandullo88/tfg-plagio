f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################
## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################

load("Dataset/Sintetico/repaso1.RData")

lugar # las 7 zona
fecha # las 7 fechas
telefonos # 49 valores donde encontramos primero las zonas de 7 en 7, y dentro de cada zona los distintos anos

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.


## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# zona asociada a cada dato del vector telefonos
lugar.factor=rep(lugar,each=7)
lugar.factor=factor(lugar.factor)
lugar.factor  # al final lo que obtenemos es un factor de 49 datos, con 7 niveles donde, los 7 primeros datos
# corresponden a la primera zona, los siguientes 7 a la segunda zona, y asi sucesivamente.

# fecha asociada a cada dato del vector telefonos
fecha.factor=rep(fecha,7)
fecha.factor=factor(fecha.factor)
fecha.factor # aqui lo que hemos obtenido es un factor de 49 datos, con 7 niveles, pero con la diferencia al anterior
# de que este tiene los 7 niveles en los primeros 7, y asi en el resto, ya que corresponde a las fechas en
# cada zona. Es decir, el primer dato corresponde al ano 1951 en N.America



## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.
## ###################################################################

# Con la instruccion 'fecha.factor== "1961"' se consigue los valores donde la fecha es 1961
telefonos1961=telefonos[fecha.factor == "1961"]# vector con los numeros de lineas telefonicas en el ano 1961, en cada zona
telefonos1961
names(telefonos1961)=lugar
telefonos1961



## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.


# Proceso de construccion de los porcentajes de cada zona paso a paso

telefonosNAmerica=telefonos[1:7]
telefonosEurope=telefonos[8:14]
telefonosAsia=telefonos[15:21]
telefonosSAmerica=telefonos[22:28]
telefonosOceania=telefonos[29:35]
telefonosAfrica=telefonos[36:42]
telefonosMidAmerica=telefonos[43:49]

tablatelZon=rbind(telefonosNAmerica,telefonosEurope,telefonosAsia,telefonosSAmerica,
                  telefonosOceania,telefonosAfrica,telefonosMidAmerica)
tablatelZon
colnames(tablatelZon)=fecha
tablatelZon

sumazonas=apply(tablatelZon,1,sum)
sumazonas

porcentaje1961=100*telefonos1961/sumazonas
porcentaje1961

# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:

# Lectura datos

datos=read.table("Dataset/Sintetico/estadisticas_vitales.txt")
datos
Ano = rownames(datos)

# 1. La poblacion en el ano 1992..

pob1992=datos[Ano==1992,"Poblacion"] #para seleccionar los datos usamos corchetes , en la primera parte seleccionamos las filas y en la segunda las columnas
pob1992



# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".

# Se crea la funcion 'crecimieto' con un parametro 'anho', siendo anho el ano del que se quiere obtener
# el crecimiento vegetativo. Se crea un condicional if, si anho < 1948 o anho>1996, retorna la frase
# "Solo disponibles para los ans entre 1948 y 1996". En c.c. se calcula el crecimiento vegetativo
# (nacimiento - defunciones). Idem como el aparatdo anterior 'datos[Ano==anho, "N.nacimientos"]' - 
# datos[Ano==anho,"N.defunciones"].
}
crecimientoAno= function(Anho){
  crecveget=datos[Ano==Anho,"N.nacimientos"]-datos[Ano==Anho,"N.defunciones"]
    if(Anho >1996){
     cat("Solo disponibles para los anos entre 1948 y 1996") }
    else if (Anho < 1948){
     cat("Solo disponibles para los anos entre 1948 y 1996") }
    else{
      crecveget 
    }
}
f2= function(){
# Comprobaciones
crecimientoAno(1984)
crecimientoAno(1951)
crecimientoAno(1920)
crecimientoAno(2014)



# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

# Columna Nacimientos
nacimientos=datos[,2]
names(nacimientos)=Ano
nacimientos

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

# Al marco de datos aplicamos en el selector de fila la condicion 'Ano==1960' y en el de columna
# -1 ya que se devuelce Poblacion, Nacimientos y Defunciones

datos1960=datos[Ano==1960, ]
datos1960

# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.


## Del marco de datos le pedimos los datos desde la fila de inicio
## a la fila final ("ini:fin").


pos1950=which(rownames(datos)==1950) ## con which obtenemos la fila del ano de inicio.
pos1989=which(rownames(datos)==1989) ## con which obtenemos la fila del ano de fin.
datos1950_1989=datos[pos1950:pos1989,] ## Del marco de datos le pedimos los datos desde la fila de inicio
                                      ## a la fila final ("pos1950:pos1989").
datos1950_1989

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

pos1960=which(rownames(datos)==1960) # Obtenemos la fila para 1960
pos1970=which(rownames(datos)==1970) # Obtenemos la fila para 1970
pos1980=which(rownames(datos)==1980) # Obtenemos la fila para 1980
pos1990=which(rownames(datos)==1990) # Obtenemos la fila para 1990
datos4=datos[c(pos1960,pos1970,pos1980,pos1990),c(2,3)] # Obtenemos los datos deseados
datos4}
