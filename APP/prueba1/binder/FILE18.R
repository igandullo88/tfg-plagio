f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################
## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

load("repaso1.RData")


## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

f.fecha=factor(rep(fecha, times= 7)) # He creado el factor "f.fecha" para el numero de telefonos en las diferentes zonas a
                                     # partir de la repeticion 7 veces del vector fecha que se consigue a traves de la funcion "rep" y el atributo "times"

f.lugar=factor(rep(lugar, each= 7))  # He creado el factor "f.lugar" respecto al numero telefonos en los diferentes anos
                                     # usando el argumento "each" que an traves de la funcion "rep", y repite cada dato del vector que se le pasa, tantas veces como valga el atributo each


## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.
## ###################################################################

telefonos1961 = telefonos[which(f.fecha==1961)]     # which solo nos admite valores logicos y nos devolvera la posicion de los TRUE,
                                                    # luego cuando le pasamos el doble igual al f.fecha nos devuelve un vector logico 
                                                    # de si el valor del vector cooresponde  al que hemos igualado
names(telefonos1961)<-f.lugar[which(f.fecha==1961)] #seleccinamos de la misma forma qeu antes los datos que contiene el f.lugar, 
                                                    #zona que correspondera a cada dato de telefonos1961
telefonos1961 

#no estoy seguro pero creo que podria hacerse en una sola linea

## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

porcentaje1961= (telefonos1961/sum(telefonos1961))*100 # haciendo el producto de cada dato del telefonos de 1961 entre la suma de todos los telefonos de 1961, aplicando "sum" al vector, por 100 obtenemos el porcentajede telefonos1961
porcentaje1961



# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:


# El fichero estadisticas_vitales.txt contiene una tabla de datos relativos a las estadisticas vitales genericas para la poblacion espanola. La tabla consta de 49 filas, cada una de las cuales se corresponde con un ano, desde 1948 hasta 1996. Para cada fila existen 3 columnas que contienen la siguiente informacion:
# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.
# Se pide construir un marco de datos con el contenido del fichero anterior y escribir expresiones de R que seleccionen del mismo la siguiente informacion:
est_vitales<- read.table("estadisticas_vitales.txt", fileEncoding = "UTF-8")
head(est_vitales)
attach(est_vitales)

nombres<-names(est_vitales)
years<-row.names(est_vitales)

# 1. La poblacion en el ano 1992.

Poblacion[which(row.names(est_vitales)==1992)]


# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".


#crecimiento vegetativo = tasa de natalidad - tasa de mortalidad
#tasa de natalidad= (nacimientoAno/PoblacionTotalAno) *1000
}
crecimientoAno <- function(year) {
  datos<-       est_vitales[which(row.names(est_vitales)==year),]
  resultado<-   ((datos[,2]-datos[,3])/datos[,1])*100 
  
  if ( sum(as.integer(years)==year) == 1 ) {
    resultado
  } else {
      warning("Solo disponibles para los anos entre 1948 y 1996")
    }
}
f2= function(){
crecimientoAno(1948)


# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

Nacimientos #el attach hecho antes permite a los vectores columna simplemente dando sus nombres
est_vitales[,c("Nacimientos")] #otra forma

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.
 
 est_vitales["1960",] #tambien est_vitales[as.character(1960),]


# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.
 
 est_vitales[as.character(1950:1989),]
 

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.
 
 b = seq(1950,1990,by=10)
 est_vitales[as.character(b),c("Nacimientos", "Defunciones")]


detach(est_vitales)
 
 }
