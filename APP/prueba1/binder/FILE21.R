f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################
## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

load("repaso1.RData")

# Creamos un factor y repetimos cada dato del vector lugar 7 veces.
zona<-factor(rep(lugar,each=7),levels = c("N.Amer", "Europe", "Asia", "S.Amer", "Oceania", "Africa", "Mid.Amer"))
# Creamos un factor y repetimos el vector fecha 7 veces.
ano<-factor(rep(fecha,7))
cbind(telefonos,zona,ano)

## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.
## ###################################################################

# Con la instruccion "telefonos[ano==1961]" obtenemos los telefonos en 1961
telefonos1961<-telefonos[ano==1961]
names(telefonos1961) <- levels(zona) # Asignamos nombres con la func names, proporcionandoles los lvls de zona
telefonos1961

## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

# Como ya tenemos el vector telefonos1961 creado, tan solo tenemos que / por la suma del vector telefonos1961 y multiplicarlo por 100.
porcentaje1961<-(telefonos1961/sum(telefonos1961))*100
porcentaje1961

sum(porcentaje1961) #Tiene que salir 100

# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:

# Lectura datos
datos2<-read.table("Dataset/Sintetico/estadisticas_vitales.txt",header = T)
names(datos2)
Ano = rownames(datos2)
# 1. La poblacion en el ano 1992.

pob1992<-datos2[Ano == "1992",1]  # Indicamos que la fila deseada "Ano==1992"
                                  # Columna 'Poblacion'
pob1992

# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996"

# Se crea la funcion 'crecimietoAnno' con un parametro 'anno', siendo anno el ano del que se quiere obtener
# el crecimiento vegetativo. Se crea un condicional if, si anno < 1948 o anno >1996, retorna la frase
# "Solo disponibles para los ans entre 1948 y 1996". En c.c. se calcula el crecimiento vegetativo
# (nacimiento - defunciones) con la instruccion 'datos[Anno, 2]' - datos[Anno,3].


attach(datos2)}
crecimientoAnno<-function(anno){
  anno<-as.character(anno)
  if(anno >="1948"&& anno<="1996") {
    return(datos2[anno,2]-datos2[anno,3])
  }
  else {
    print("Solo disponibles para los anos entre 1948 y 1996")
  }
}
f2= function(){
# Comprobaciones

crecimientoAnno(1996)
crecimientoAnno(1000)

# 3. El numero de nacimientos desde el primer hasta el ultimo ano

# Aplicamos la funcion sum a la columna Nacimientos
sum(Nacimientos)

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

# Al marco de datos aplicamos en el selector de fila la condicion 'Ano==1960' y en el de columna
# -1 ya que se devuelce Poblacion, Nacimientos y Defunciones
datos2[Ano == "1960", -1]

# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

# Al marco de datos aplicamos en el selector de fila la condicion 'Ano %in% seq(1950,1989))'

datos2[Ano %in% seq(1950,1989), ]

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

# Al marco de datos aplicamos en el selector de fila la condicion 'Ano %in% seq(1950,1990,10))' y en el de columna
# -2 ya que se devuelve Nacimientos y Defunciones

datos2[Ano %in% seq(1950,1990,10), -2]

detach(datos2)
}
