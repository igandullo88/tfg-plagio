f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.
## ###################################################################

## Ejercicio 1
## ###################################################################
## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.
## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

region = factor(lugar, rep(7,7)) # Creo los factores.
tiempo = factor(fecha, 7)



## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.
phone67 = telefonos[which(tiempo == "1961")] # telefonos en 1961. which marca la posicion donde se cumple 
names(phone67) = region[which(tiempo == "1961")] # Se anaden los nombres


## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.



prop.table(phone67) # Probabilidades del vector anterior, con prop.table

######################################################################
# Ejercicio 5
# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:
# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.
# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:

datos <- read.table("Dataset/Sintetico/estadisticas_vitales.txt",fileEncoding = "utf-8")

# 1. La poblacion en el ano 1992.

datos["1992", "Poblacion"] # Poblacion (columna) en 1992 (fila), 

# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".
## Definimos la funcion con la instruccion "function()" y le exigimos 
## el argumento "ano", para el ano.

}
crecimiento = function( k){
  datos[k, 2] - datos[k,3] # nacimeintos menos defunciones en un ano k
}
f2= function(){


# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

sum(datos[,2]) # nacimientos coluna dos, se suman

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

datos["1960",] # fila 1960.


# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

datos[paste(1950:1989),] # filas de 1950 a 1989 se hace un paste para que se considere texto
                         # tambien, se puede hacer as.character

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

datos[as.character(seq(1950,1990,10)),] # Igual que antes, con as.character
}
