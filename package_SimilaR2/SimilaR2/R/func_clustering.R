#' @title Análisis clustering jerarquico completo
#' @description Realiza un análisis clustering jerárquico completo, proporcionando resultados del tipo de método utilizado,
#' silueta,  dendograma, ...
#' @param M matriz de distancia simétrica para el anñalisis clústering
#' @param  cut_k número de clúster.
#' @param agglomerative TRUE / FALSE. Si TRUE se aplica el método jerárquico de aglomeración. En caso contrario, se
#' se aplica método jerárquico de división.
#' #' @examples
#' clustering(M, 3)
#' clustering(M2, 4, agglomerative = FALSE)
#' @export clustering
clustering <- function(M, cut_k, agglomerative = TRUE){

  if(nrow(M)<=cut_k) stop("The number of clusters cannot be greater than the number of files.")

  if(agglomerative == TRUE){
    m <- c( "average", "single", "complete", "ward")
    names(m) <- c( "average", "single", "complete", "ward")

    ac <- function(x) {
       cluster::agnes(M, method = x)$ac
    }
    res <- purrr::map_dbl(m, ac)
    method_max <- res[res == max(res)]
    nam_res <- names(res)[res == method_max]


    hc <- cluster::agnes(M, method = nam_res)
    clust <- cutree(hc, k = cut_k)
    out_graf <- factoextra::fviz_cluster(list(data = M, cluster = clust))

    if(nam_res == "ward") nam_res = paste0(nam_res,".D")

    out_graf2 <-factoextra::hcut(M, k = cut_k, hc_func = "agnes", hc_method = nam_res) %>%
                factoextra::fviz_dend(rect = TRUE, cex = 0.5)

    out_graf3 <- factoextra::hcut(M, k = cut_k, hc_func = "agnes", hc_method = nam_res) %>%
                 factoextra::fviz_silhouette()

    dat =  out_graf3$data %>%
           dplyr::group_by(cluster)

    tab_size = forcats::fct_count(dat$cluster) %>%
               dplyr::rename(cluster = f, size = n)
    tab_silhouette = dat  %>% dplyr::summarise(ave.sil.width = round(mean(sil_width),2))
    tab_sil = dplyr::inner_join(tab_size, tab_silhouette, id = "cluster")


    out <- c(paste0("The best method of agglomeration is to:", nam_res),
             paste0("Agglomeration coefficient:", round(method_max,4)),
             paste0("Average silhouette width:", round(mean(out_graf3$data$sil_width),2)))

    out <- dplyr::as_data_frame(out)%>%
           tidyr::separate(col =1, sep = ":", into = c(" ", "Resultado"))


    list(coef = out,
         dispersion_diagram= out_graf, Dendogram =  out_graf2,
         Silhouette_graph = out_graf3,
         Res_Silhouette = tab_sil)
  }else{
    if(agglomerative  == FALSE){
      hc <- cluster::diana(M)

      clust <- cutree(hc, k = cut_k)
      out_graf <- factoextra::fviz_cluster(list(data = M, cluster = clust))

      out_graf2 <-factoextra::hcut(M, k = cut_k, hc_func = "diana") %>%
                  factoextra::fviz_dend(rect = TRUE, cex = 0.5)
      out_graf3 <- factoextra::hcut(M, k = cut_k, hc_func = "diana") %>%
                   factoextra::fviz_silhouette()

      dat =  out_graf3$data %>%
             dplyr::group_by(cluster)

      tab_size = forcats::fct_count(dat$cluster) %>%
                 dplyr::rename(cluster = f, size = n)
      tab_silhouette = dat  %>% dplyr::summarise(ave.sil.width = round(mean(sil_width),2))
      tab_sil = dplyr::inner_join(tab_size, tab_silhouette, id = "cluster")

      out <- c(paste0("Division coefficient:", round(hc$dc,4)),
               paste0("Average silhouette width:", round(mean(out_graf3$data$sil_width),2)))

      out <- dplyr::as_data_frame(out)%>%
             tidyr::separate(col =1, sep = ":", into = c(" ", "Resultado"))

      list(coef = out,
           dispersion_diagram= out_graf, Dendogram =  out_graf2,
           Silhouette_graph = out_graf3,
           Res_Silhouette = tab_sil)

    }
  }
}


# cl_ex = clustering(M = Matriz_win, cut_k = 5,agglomerative = TRUE)
# cat(cl_ex$coef)
# cl_ex$dispersion_diagram
# cl_ex$Dendogram

# require(factoextra)
# require(cluster)
# require(dendextend)
