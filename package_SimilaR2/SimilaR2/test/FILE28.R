# ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.
## ###################################################################

## Ejercicio 1
## ###################################################################
## El vector telefonos contiene el número de lineas telefonicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.
## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# Utilio la función factor del paquete base de R para convertir los vectores
# zona y fecha en clase factor. Para el factor lugar se utiliza dentro de la
# func factor, la función rep() con el parámetro each = 7 (vector lugar reptido 7 veces).
# Y para la fecha el parámetro es time = 7 (mismo año 7 veces)

rep_zona = rep(lugar, each= 7)
rep_anual = rep(fecha, times= 7)

f.zona = factor(rep_zona)  
f.anual = factor(rep_anual) 


## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.

# Utilizo un vector dicotómico defindido en anual1961. Donde en el factor 
# f.anual sea igual a 1961 devolvera 1 y 0 c.c.
anual1961 = f.anual == "1961"
telefonos1961 = telefonos[anual1961] # Selecciona los valores del vector telefonos
                                     # que cumplan la condición impuesta en anual1961

# Aplico sobre el vector creado la función names() igualándola a los valores de f.zona
# donde el vector anual1961 es TRUE
names(telefonos1961) = f.zona[anual1961]

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.

# Solo existe un valor por zona  en el año 1961, basta con dividir el vector
# creado en el apartado anterior por la suma del mismo, y multiplicarlo por cien.

Total = sum(telefonos1961)

telefonos1961 / Total * 100

######################################################################
# Ejercicio 5
# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:
# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.
# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:

estadisticas_vitales <- read.csv("estadisticas_vitales.txt", header = T, 
                        encoding = "UTF-8", sep = "")

anualidades <- rownames(estadisticas_vitales)

# El marco de datos tiene tres columnas, la primera es la población total, 
# la segunda los nacimientos y la tercera las defunciones.

# 1. La población en el año 1992.

# Creo el vector TRUE / FALSE  nombrado como a1992, donde si en el vector anualidad es 1992 
# devuelve un valor uno y en c.c. cero.

a1992 = anualidades == "1992"
estadisticas_vitales[a1992]  # Retorna los valores de estadisticas_vitales donde a1992 es igual a uno.

# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".
## Definimos la función con la instrucción "function()" y le exigimos 
## el argumento "ano", para el año.

# He creado la fucnión crecimientoAño, que recibe un parámetro x 
# donde x es la anualidad para el calculo del crecimiento vegetativo
# Si x esta en el intervalo del vector anualidad:
#   - Se calcula un variable bernoulli anualidad == x, y seleciono los valores
#     de naciemientos y defunciones donde esa varaible es igual a TRUE
#     y de retorna la resta de  nacidos menos fallecidos.
# Si no cumple la condición devuelve la frase dictada en el enunciado.

crecimientoAño <- function(x){
  if(x %in% anualidades){
    attach(estadisticas_vitales)
    sel = anualidades == x
    return(N.nacimientos[sel] - N.defunciones[sel])
    detach(estadisticas_vitales)
  }else{
    stop("Sólo disponibles para los años entre 1948 y 1996")
  }
}

# 3. El número de nacimientos desde el primer hasta el último año.

# La solución se obtiene sumando todos los valores de la columna N.nacimientos
# Al ser estadisticas_vitales un data.frame se puede seleccionar la columna N.nacimientos
# escribiendo dicho nombre entre comilla en el selector de columna [,"Nombre"].
# Por último, se suman todos los valores con la función sum.

nacimientos = estadisticas_vitales[,"N.nacimientos"]
sum(nacimientos)

# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.

# Idem como en apartado anteriores, vector bernoulli con la instrucción anualidades == 1960
a1960 = anualidades == "1960"
# En el selector fila, introducimos el vecotr a1960. Y seleccionamos todas las columnas
# Población, N.nacimientos y N.defunciones.

estadisticas_vitales[a1960, ]

# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.

# Idem anterior, cambiando que el vector dicotómico es las anualidades que estén en el vector
# de los anales entre mil noveciento cincuenta y mil novecientos ochenta y nueva
a5089 = anualidades %in% c(1950:1989)
estadisticas_vitales[a5089, ]
# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.
# En este caso, el vector bernoulli son los valores del vector anualidades donde se encuentren
# las decadas comprendidas entre 1950 a 1990. Y se seleccionan las columnas N.nacimientos y N.defunciones
# en el selector de columnas.
a50.90 = anualidades %in% c(seq(1950,1990,10))
estadisticas_vitales[a50.90, c("N.nacimientos", "N.defunciones") ]
