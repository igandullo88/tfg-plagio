## ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el número de líneas telefónicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.

## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# En primer lugar cargamos el fichero
load("repaso1.RData")

# Basta repetir cada lugar length(fecha) veces y
# repetir el vector fecha length(lugar) veces (hasta llenar length(telefonos))
f.fechas <- as.factor(rep_len(fecha, length(telefonos)))
f.lugares <- as.factor(rep(lugar, each=length(fecha)))


## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.

# Filtramos el vector usando f.fechas y le asignamos los nombres con `names<-`
telefonos1961 <- telefonos[f.fechas == "1961"]
names(telefonos1961) <- f.lugares[f.fechas == "1961"]


## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.

# La proporción se obtiene diviendo el vector telefonos1961 entre su suma.
porcentaje1961 <- telefonos1961/sum(telefonos1961)


# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:

# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:

# Cargamos los datos con readtable
tabla <- read.table("estadisticas_vitales.txt")

# 1. La población en el año 1992.

tabla["1992","Población"] # Población en el año 1992. 
# Para esta salida, se seleccionan en el corchete de selección de elementos [fila, columna]
# Donde [fila ="1992", columna = "Poblacion"]

# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".

## Definimos la función con la instrucción "function()" y le exigimos 
## el argumento "ano", para el año.

crecimientoAño <- function(año) {
  t <- as.character(año)
  if(!(t %in% rownames(tabla)))  # Condicional si t no esta en el vector de las anualidades (nombres de las filas de tabla)
    stop(sprintf("Solo disponibles para los años entre %s y %s",
                 min(rownames(tabla)),
                 max(rownames(tabla))))
  # devuelve el crecimiento vegetativo
  tabla[t, "Nacimientos"] - tabla[t, "Defunciones"] 
  # Para el calculo del crecimiento vegetativo. Obtenemos los nacimientos en el año t y las
  # defunciones en el año t, y se restan nacimientos menos defunciones.
}

# Si se desea la tasa de crecimiento vegetativo, en lugar de su valor absoluto
# hay que dividir el resultado anterior por la población

tasaCrecimientoAño <- function(año) crecimientoAño(año) / tabla[as.character(año), "Población"]

# 3. El número de nacimientos desde el primer hasta el último año.

sum(tabla$Nacimientos) # Sumo los datos de la columna Nacimeintos

# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.

tabla["1960",] # En el selector fila se introduce 1960. Salida de las tres columnas del dataframe tabla
               # En el anal 1960

# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.

tabla[as.character(1950:1989),] # En el selector fila se introduce as.character(1950:1989). Salida de 
# todas las columnas de tabla y las filas de 1950 a 1989.

# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.

# Introduzo en el selector de fila introduzco as.character(seq(1950, 1990, by=10)). 
# Con esto, consigo las filas de las decadas de 1950 a 1990.
tabla[as.character(seq(1950, 1990, by=10)),]
