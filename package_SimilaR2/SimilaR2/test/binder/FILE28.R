f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.
## ###################################################################

## Ejercicio 1
## ###################################################################
## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.
## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# Utilio la funcion factor del paquete base de R para convertir los vectores
# zona y fecha en clase factor. Para el factor lugar se utiliza dentro de la
# func factor, la funcion rep() con el parametro each = 7 (vector lugar reptido 7 veces).
# Y para la fecha el parametro es time = 7 (mismo ano 7 veces)

rep_zona = rep(lugar, each= 7)
rep_anual = rep(fecha, times= 7)

f.zona = factor(rep_zona)  
f.anual = factor(rep_anual) 


## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.

# Utilizo un vector dicotomico defindido en anual1961. Donde en el factor 
# f.anual sea igual a 1961 devolvera 1 y 0 c.c.
anual1961 = f.anual == "1961"
telefonos1961 = telefonos[anual1961] # Selecciona los valores del vector telefonos
                                     # que cumplan la condicion impuesta en anual1961

# Aplico sobre el vector creado la funcion names() igualandola a los valores de f.zona
# donde el vector anual1961 es TRUE
names(telefonos1961) = f.zona[anual1961]

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

# Solo existe un valor por zona  en el ano 1961, basta con dividir el vector
# creado en el apartado anterior por la suma del mismo, y multiplicarlo por cien.

Total = sum(telefonos1961)

telefonos1961 / Total * 100

######################################################################
# Ejercicio 5
# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:
# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.
# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:

estadisticas_vitales <- read.csv("estadisticas_vitales.txt", header = T, 
                        encoding = "UTF-8", sep = "")

anualidades <- rownames(estadisticas_vitales)

# El marco de datos tiene tres columnas, la primera es la poblacion total, 
# la segunda los nacimientos y la tercera las defunciones.

# 1. La poblacion en el ano 1992.

# Creo el vector TRUE / FALSE  nombrado como a1992, donde si en el vector anualidad es 1992 
# devuelve un valor uno y en c.c. cero.

a1992 = anualidades == "1992"
estadisticas_vitales[a1992]  # Retorna los valores de estadisticas_vitales donde a1992 es igual a uno.

# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".
## Definimos la funcion con la instruccion "function()" y le exigimos 
## el argumento "ano", para el ano.

# He creado la fucnion crecimientoAno, que recibe un parametro x 
# donde x es la anualidad para el calculo del crecimiento vegetativo
# Si x esta en el intervalo del vector anualidad:
#   - Se calcula un variable bernoulli anualidad == x, y seleciono los valores
#     de naciemientos y defunciones donde esa varaible es igual a TRUE
#     y de retorna la resta de  nacidos menos fallecidos.
# Si no cumple la condicion devuelve la frase dictada en el enunciado.
}
crecimientoAno <- function(x){
  if(x %in% anualidades){
    attach(estadisticas_vitales)
    sel = anualidades == x
    return(N.nacimientos[sel] - N.defunciones[sel])
    detach(estadisticas_vitales)
  }else{
    stop("Solo disponibles para los anos entre 1948 y 1996")
  }
}
f2= function(){
# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

# La solucion se obtiene sumando todos los valores de la columna N.nacimientos
# Al ser estadisticas_vitales un data.frame se puede seleccionar la columna N.nacimientos
# escribiendo dicho nombre entre comilla en el selector de columna [,"Nombre"].
# Por ultimo, se suman todos los valores con la funcion sum.

nacimientos = estadisticas_vitales[,"N.nacimientos"]
sum(nacimientos)

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

# Idem como en apartado anteriores, vector bernoulli con la instruccion anualidades == 1960
a1960 = anualidades == "1960"
# En el selector fila, introducimos el vecotr a1960. Y seleccionamos todas las columnas
# Poblacion, N.nacimientos y N.defunciones.

estadisticas_vitales[a1960, ]

# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

# Idem anterior, cambiando que el vector dicotomico es las anualidades que esten en el vector
# de los anales entre mil noveciento cincuenta y mil novecientos ochenta y nueva
a5089 = anualidades %in% c(1950:1989)
estadisticas_vitales[a5089, ]
# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.
# En este caso, el vector bernoulli son los valores del vector anualidades donde se encuentren
# las decadas comprendidas entre 1950 a 1990. Y se seleccionan las columnas N.nacimientos y N.defunciones
# en el selector de columnas.
a50.90 = anualidades %in% c(seq(1950,1990,10))
estadisticas_vitales[a50.90, c("N.nacimientos", "N.defunciones") ]}
