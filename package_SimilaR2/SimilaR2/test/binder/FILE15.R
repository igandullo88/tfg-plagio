f1= function(){## ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.
load("Dataset/Sintetico/repaso1.RData")

lugar <- rep(factor(lugar),each=7)  # Creamos un factor y repetimos cada dato del vector lugar 7 veces.
fecha <- rep(factor(fecha),7) # Creamos un factor y repetimos el vector fecha 7 veces.
factor_lugar <- factor(lugar)
factor_lugar
factor_fecha <- factor(fecha)
factor_fecha

## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.

# Con la instruccion "telefonos[factor_fecha==1961]" obtenemos los telefonos en 1961
telefono1961 <- telefonos[factor_fecha==1961]
# y ya solo queda ponerle los nombres de los lugranes a los telefonos, le adjudicamos nombres al vector con names(telefonos1961)
# y le damos los nombres de las ciudades de los datos de 1961, de la misma forma que antes "factor_lugar[factor_fecha==1961]"
names(telefono1961) <- factor_lugar[factor_fecha==1961]
telefono1961

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

# Como ya tenemos el vector telefonos1961 creado, tan solo tenemos que / por la suma del vector telefonos1961 y multiplicarlo por 100.
porcentaje1961 <- telefono1961/sum(telefono1961)
porcentaje1961
pie(porcentaje1961,col=rainbow(7)) # Se muestra un grafico de sectores

# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:

# Leemos los datos del  archivo "estadisticas_vitales.txt" mediante la funcion read.csv

# Lectura de datos
marco_datos= read.csv("Dataset/Sintetico/estadisticas_vitales.txt",fileEncoding="utf-8" ,sep="")
marco_datos
Ano = as.numeric(rownames(marco_datos))

# A continuacion seleccionaremos la informacion que se nos pide mediante expresiones de R:

# 1. La poblacion en el ano 1992

poblacion1992=marco_datos["1992",1] ## Tomo la columna 1 que es la de la
# poblacion total
poblacion1992

# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".

# Se crea la funcion 'crecimietoAno' con un parametro 'ano', siendo ano el ano del que se quiere obtener
# el crecimiento vegetativo. Se crea un condicional if, si ano %in% Ano, retorna el crecimiento vegetativo
# (nacimiento - defunciones); Idem como el aparatdo anterior 'datos[Ano==ano, "N.nacimientos"]' - 
# datos[Ano==x,"N.defunciones"]. En c.c. devuelve un print con la frase
# "Solo disponibles para los anos entre 1948 y 1996"

}
crecimientoAno <- function(ano){
  crec_veg= marco_datos[Ano == ano,"N.nacimientos"]-marco_datos[Ano == ano,"N.defunciones"]
  if(ano %in% Ano){
    return(crec_veg)
  }
  else{
    print("Solo disponibles para los anos entre 1948 y 1996")
  }
}
f2= function(){
# Comprobaciones

crecimientoAno(192)
crecimientoAno(1950)

# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

# Aplicamos la funcion sum a la columna Nacimientos
nacimientos_totales=sum(marco_datos[,2])
nacimientos_totales

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

# # Al marco de datos aplicamos en el selector de fila la condicion 'Ano==1960' y en la de columna
# Poblacion, Nacimientos y Defunciones
poblacion1960=marco_datos[Ano == 1960, "Poblacion"]
nacimientos1960=marco_datos[Ano == 1960,"N.nacimientos"]
defuncion1960=marco_datos[Ano == 1960,"N.defunciones"]
poblacion1960
nacimientos1960
defuncion1960

# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

# Se calcula una var para guardar la seq(1950,1989)
# Al marco de datos aplicamos en el selector de fila la condicion 'anos1950_1989' y en la de columna
# Poblacion, Nacimientos y Defunciones

anos1950_1989=seq(1950,1989)
poblacion1950_1989=marco_datos[as.character(anos1950_1989),1]
nacimientos1950_1989=marco_datos[as.character(anos1950_1989),2]
defunciones1950_1989=marco_datos[as.character(anos1950_1989),3]
poblacion1950_1989
nacimientos1950_1989
defunciones1950_1989

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

# Se calcula una var para guardar la seq(1950,1990,10)
# Al marco de datos aplicamos en el selector de fila la condicion 'anos' y en la de columna
# Nacimientos y Defunciones

anos=seq(1950,1990,10)
nacimiento_0=marco_datos[as.character(anos),2]
nacimiento_0
defunciones_0=marco_datos[as.character(anos),3]
defunciones_0}
