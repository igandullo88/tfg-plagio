f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################
## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.


load("repaso1.RData")

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.

## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha

# Factor con la zona asociada a cada dato del vector telefonos
factorLugar=rep(lugar,each=7)
factorLugar=factor(factorLugar,levels=lugar)
factorLugar

# Factor con la fecha asociada a cada dato del vector telefonos
factorFecha=rep(fecha,times=7)
factorFecha=as.factor(factorFecha)
as.character(factorFecha)

################################################
datos = cbind(as.character(factorLugar),as.character(factorFecha),telefonos)
colnames(datos)=c("Lugar","Ano","Telefono")
datos 
# He creado una matriz con 3 columnas, donde la primera columna corresponde al pais, la segunda al ano y la tercera a la
# cantidad de telefonos en cada ano en cada pais.

## ###################################################################
## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.
## ###################################################################



# Hacer sin data.frame
telefonos1961=datos[datos[,2]==1961,3]    # Extraigo de los datos creados anteriormente, la columna telefono con los datos correspondientes al ano 1961.
telefonos1961=as.numeric(telefonos1961)   # Paso los datos a tipo numerico, ya que al introducirlos en la matriz los leia como tipo "character".

names(telefonos1961)=datos[datos[,2]==1961,1] # Los nombro con el nombre del pais al que corresponde cada uno.
telefonos1961 

## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.


porcentaje1961=telefonos1961/sum(telefonos1961) # Calculo el porcentaje que representan cada numero del vector anterior, que son la cantidad de lineas en cada pais en ese ano.
names(porcentaje1961)=datos[datos[,2]==1961,1]  # Vuelvo a nombrar cada porcentaje con el pais al que corresponde
porcentaje1961 

barplot(porcentaje1961,cex.names=0.8,col=c(1:7),main= "Barplot de Porcentaje1961") # Anado una visualizacion de estos porcentajes
pie(porcentaje1961, clockwise=F, main="Diagrama Sectores Porcentaje1961",col=c(1,2,3,4,5,6,7),
    border = 1, cex.names=1)  # Anado tambien una representacion mediante diagrama de sectores.






# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:

# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:



# Importacion de los datos:

datos = read.csv("estadisticas_vitales.txt",encoding="UTF-8",sep="")
# Selecciono encoding="UTF-8" para que lea bien los nombres con tilde. "sep" para que separe los datos, 
# si no usamos "sep", lo lee como un unico vector columna y no como una matriz con 3 columnas. 
summary(datos)
View(datos)
class(datos)

# 1. La poblacion en el ano 1992.

poblacion1992=datos["1992",1] # Selecciono el valor de la fila nombrada como 1992 (que es el ano que se pide) y
# de la columna 1 que es la correspondiente a la variable poblacion
poblacion1992

# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".


}
CrecimientoAno=function(ano){
  if (ano < 1948 || ano > 1996) {  # Comprueba si el ano no pertenece a uno de los comprendidos entre 1948 y 1996.
    print("Error.Solo disponibles para los anos entre 1948 y 1996") # Mensaje de alerta de error, ano no valido.
  }
  else {
    ano=as.character(ano) # Transforma el numero en un tipo "character"
    c = ((datos[ano,2]-datos[ano,3])/datos[ano,1]) * 100 # Calculo del crecimiento vegetativo en el ano seleccionado.
    print(paste("Crecimiento Vegetativo de", ano ,"=  " , c))  # Valor que devuelve la funcion
  }
}
f2= function(){
CrecimientoAno(1940) # Prueba metiendo un un ano que no pertenece al de los datos
CrecimientoAno(1980) # Prueba con un ano que si pertenece a los datos
CrecimientoAno("1980") # Prueba con un ano que si pertenece a los datos, introduciendolo como tipo character.

################################################################################
# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

# Entiendo que lo que pide el enunciado es el numero de nacimientos totales en los anos comprendidos en los datos
NacimientosTotales=sum(datos[,2]) #Simplemente hago la suma de la segunda columna.
NacimientosTotales
# Al usar el comando read.csv se implementan los datos directamente como un data.frame, si se tuviera una
# matriz, el calculo se haria exactamente igual.

################################################################################
# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

datos1960=datos["1960",] # Selecciono la fila correspondiente a 1960, y los valores de las 3 columnas. Si se tuviese una matriz se haria de igual forma.
datos1960

################################################################################
# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

anos_5=c(1950:1989) # Creo un vector con los anos que pide 
datos1950_1989=datos[as.character(anos_5),] # Convierto el vector anterior a tipo "character" y selecciono del conjunto de datos aquellas filas que corresponden con los 
# anos seleccionados en el vector anterior
datos1950_1989

################################################################################
# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

anos_6=seq(1950,1990,by=10) # Genero un vector con los anos comprendidos entre 1950 y 1990 de 10 en 10, es decir, la funcion coge el ano 1950, el 1950+10, 1950+2*10, ... 
# asi hasta 1990
datos_6=datos[as.character(anos_6),c(2:3)] # Igual que en el ejercicio anterior, selecciono las filas correspondientes a estos anos generados en el vector anterior,
# pero ahora solo selecciono las columnas 2 y 3, que son las correspondientes a nacimientos y defunciones.
datos_6


































}
