# ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

load("repaso1.RData")
library(dplyr)


## Ejercicio 1
## ###################################################################
## El vector telefonos contiene el número de lineas telefonicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.
## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.
## ###################################################################

# hemos construido un dataframe con los vectores que se usan en este ejercicio.
# con los vectores fecha y lugar, creamos las nuevas variables. 
# Para construir estas variables hemos usado la función rep,
# utilizando el parámetro time con el vector lugar, y el parámetro each con fecha. 

df = as_data_frame(telefonos)%>%
  mutate(fecha = rep(fecha, each = 7),lugar = rep(lugar, time = 7))


## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.


#Los vectores fecha y lugar se convierten en factores con la función factor.

df = df %>%
  mutate(fecha = factor(fecha),lugar = factor(lugar))

levels(df$fecha) # niveles del factor fecha
levels(df$lugar) # niveles del factor lugar

## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.

# Para filtrar el año 1961 del marco de datos construido, usamos la función filter
# indicamos la fecha == "1961". 

df_1961 = df  %>%
            filter(fecha == "1961")%>%
            select(telefonos = value, lugar)

telefonos1961 = df_1961$telefonos # el vector teléfonos

# Para nombrar cada dato, se usamos la función names(vector) e indicamos los nombres 

names(telefonos1961) = df_1961$lugar # guardamos el vector lugar

telefonos1961 



## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.

# A partir del marco de datos creado df_1961, usamos la función sum(telefonos), para crear
# la variable total, agruapamos por la variable lugar, y con summarise
# se calcula sum(telefonos)/total * 100.

df_porcentaje1961 = df_1961 %>%
                         mutate(total = sum(telefonos))%>%
                         group_by(lugar)%>%
                         summarise(porcentaje = sum(telefonos)/total * 100)
# Para transformar de columna a vector procedemos como el apartado anterior 

porcentaje1961 = df_porcentaje1961$porcentaje
names(porcentaje1961) = df_porcentaje1961$lugar

porcentaje1961

######################################################################
# Ejercicio 5
# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:
# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.
# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:

# Para leer el archivo estadisticas_vitales.txt, se utiliza la funcion read.csv2 
# del paquete base de R, con lo sparametros sep = "" y header = T.
df2 <- read.csv2("Dataset/Sintético/estadisticas_vitales.txt", sep = "", header =T)

# 1. La población en el año 1992.

# usamos la función filter con rownames(datos) == "1992", y selecionamos 
# la columna Población con la función select.
df2 %>%
  filter(rownames(df2)=="1992")%>%
  select(Poblacion)

# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".
## Definimos la función con la instrucción "function()" y le exigimos 
## el argumento "ano", para el año.

# El primer paso de la función es crear una data.frame que filtre las columnas
# del año proporcionado en el parametro. Si esta fuera del rango 1948-1996, mostrará
# el mensaje de error.
# Si pasa la condición, se crea una nueva variable 'crecimiento_vegetativo' a partir del
# data.frame del primer paso. Esta variable es igual a N.nacimientos-N.defunciones.
# En último lugar, seleccionamos con select creciiento_vegetativo.

crecimientoAño <- function(ano){
  
  row_ano <- df2 %>%
             filter(rownames(df2)==ano)
  if(nrow(row_ano) >0){
    row_ano %>%
      mutate(crecimiento_vegetativo = N.nacimientos-N.defunciones)%>%
      select(crecimiento_vegetativo)
  }else{
    warning("Sólo disponibles para los años entre 1948 y 1996")
  }
  
}

crecimientoAño(1945)


# 3. El número de nacimientos desde el primer hasta el último año.

# con la función select seleccionamos la columna N.nacimientos

df2 %>% 
  select(N.nacimientos)

# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.

# con filter row.names(datos) == "1960" y seleccionamos las columnas de defunciones y nacimientos

df2 %>%
  filter(row.names(df2) == "1960") %>%
  select(2,3)

# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.

# Con filter indicamos row.names(datos)>"1949" y menos que "1990"

df2%>%
  filter(row.names(df2) > "1949", row.names(df2) < "1990")

# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.

# En este último apartado, también se usa filter sobre la columna row.names(datos)
# del conjunto 1950, 1960, 1970, 1980 y 1990. Se usa la expresión '%in' de dicho conjunto.

df2 %>%
  filter(row.names(df2) %in% c(as.character(seq(1950,1990, 10))))

