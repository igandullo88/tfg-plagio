# ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################
## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.


load("repaso1.RData")

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el número de lineas telefonicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.

## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha

# Factor con la zona asociada a cada dato del vector telefonos
factorLugar=rep(lugar,each=7)
factorLugar=factor(factorLugar,levels=lugar)
factorLugar

# Factor con la fecha asociada a cada dato del vector telefonos
factorFecha=rep(fecha,times=7)
factorFecha=as.factor(factorFecha)
as.character(factorFecha)

################################################
datos = cbind(as.character(factorLugar),as.character(factorFecha),telefonos)
colnames(datos)=c("Lugar","Año","Telefono")
datos 
# He creado una matriz con 3 columnas, donde la primera columna corresponde al país, la segunda al año y la tercera a la
# cantidad de teléfonos en cada año en cada país.

## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.
## ###################################################################



# Hacer sin data.frame
telefonos1961=datos[datos[,2]==1961,3]    # Extraigo de los datos creados anteriormente, la columna teléfono con los datos correspondientes al año 1961.
telefonos1961=as.numeric(telefonos1961)   # Paso los datos a tipo numérico, ya que al introducirlos en la matriz los leía como tipo "character".

names(telefonos1961)=datos[datos[,2]==1961,1] # Los nombro con el nombre del país al que corresponde cada uno.
telefonos1961 

## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.


porcentaje1961=telefonos1961/sum(telefonos1961) # Calculo el porcentaje que representan cada número del vector anterior, que son la cantidad de líneas en cada país en ese año.
names(porcentaje1961)=datos[datos[,2]==1961,1]  # Vuelvo a nombrar cada porcentaje con el país al que corresponde
porcentaje1961 

barplot(porcentaje1961,cex.names=0.8,col=c(1:7),main= "Barplot de Porcentaje1961") # Añado una visualización de estos porcentajes
pie(porcentaje1961, clockwise=F, main="Diagrama Sectores Porcentaje1961",col=c(1,2,3,4,5,6,7),
    border = 1, cex.names=1)  # Añado también una representación mediante diagrama de sectores.






# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:

# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:



# Importación de los datos:

datos = read.csv("estadisticas_vitales.txt",encoding="UTF-8",sep="")
# Selecciono encoding="UTF-8" para que lea bien los nombres con tilde. "sep" para que separe los datos, 
# si no usamos "sep", lo lee como un único vector columna y no como una matriz con 3 columnas. 
summary(datos)
View(datos)
class(datos)

# 1. La población en el año 1992.

poblacion1992=datos["1992",1] # Selecciono el valor de la fila nombrada como 1992 (que es el año que se pide) y
# de la columna 1 que es la correspondiente a la variable población
poblacion1992

# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".



CrecimientoAño=function(año){
  if (año < 1948 || año > 1996) {  # Comprueba si el año no pertenece a uno de los comprendidos entre 1948 y 1996.
    print("Error.Sólo disponibles para los años entre 1948 y 1996") # Mensaje de alerta de error, año no válido.
  }
  else {
    año=as.character(año) # Transforma el número en un tipo "character"
    c = ((datos[año,2]-datos[año,3])/datos[año,1]) * 100 # Cálculo del crecimiento vegetativo en el año seleccionado.
    print(paste("Crecimiento Vegetativo de", año ,"=  " , c))  # Valor que devuelve la función
  }
}

CrecimientoAño(1940) # Prueba metiendo un un año que no pertenece al de los datos
CrecimientoAño(1980) # Prueba con un año que sí pertenece a los datos
CrecimientoAño("1980") # Prueba con un año que sí pertenece a los datos, introduciendolo como tipo character.

################################################################################
# 3. El número de nacimientos desde el primer hasta el último año.

# Entiendo que lo que pide el enunciado es el número de nacimientos totales en los años comprendidos en los datos
NacimientosTotales=sum(datos[,2]) #Simplemente hago la suma de la segunda columna.
NacimientosTotales
# Al usar el comando read.csv se implementan los datos directamente como un data.frame, si se tuviera una
# matriz, el calculo se haría exactamente igual.

################################################################################
# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.

datos1960=datos["1960",] # Selecciono la fila correspondiente a 1960, y los valores de las 3 columnas. Si se tuviese una matriz se haría de igual forma.
datos1960

################################################################################
# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.

años_5=c(1950:1989) # Creo un vector con los años que pide 
datos1950_1989=datos[as.character(años_5),] # Convierto el vector anterior a tipo "character" y selecciono del conjunto de datos aquellas filas que corresponden con los 
# años seleccionados en el vector anterior
datos1950_1989

################################################################################
# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.

años_6=seq(1950,1990,by=10) # Genero un vector con los años comprendidos entre 1950 y 1990 de 10 en 10, es decir, la función coge el año 1950, el 1950+10, 1950+2*10, ... 
# así hasta 1990
datos_6=datos[as.character(años_6),c(2:3)] # Igual que en el ejercicio anterior, selecciono las filas correspondientes a estos años generados en el vector anterior,
# pero ahora sólo selecciono las columnas 2 y 3, que son las correspondientes a nacimientos y defunciones.
datos_6



































